package com.everis.apiprac.controller.resource;

import lombok.Data;

@Data
public class TipoProductoResource {
	private String nombre;
	private String codigo;
}
