package com.everis.apiprac.controller.resource;


import java.math.BigDecimal;

import lombok.Data;

@Data
public class ProductoIGVResource {
	private Long id;
	private String nombre;
	private String codigo;
	private BigDecimal precio; 
	private BigDecimal precioNeto; //(Calcular precio + precio*(igv)) 
	//*El igv tiene que ser consumido del .yml encontrado github
}
