package com.everis.apiprac.controller.resource;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ProductoResource {
	private Long id;
	private String nombre;
	private String codigo;
	private String descripcion;
	private BigDecimal precio;
	private TipoProductoResource tipoProducto;
	private Boolean activo;

}
