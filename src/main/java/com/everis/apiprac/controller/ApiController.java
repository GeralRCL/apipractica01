package com.everis.apiprac.controller;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.apiprac.service.ProductoService;
import com.everis.apiprac.service.TipoProductoService;

import com.everis.apiprac.controller.resource.ProductoIGVResource;
import com.everis.apiprac.controller.resource.ProductoReducidoResource;
import com.everis.apiprac.controller.resource.ProductoResource;
import com.everis.apiprac.controller.resource.TipoProductoResource;
import com.everis.apiprac.model.entity.Producto;

@RestController
public class ApiController {
	
	@Autowired
	ProductoService productoService;
	
	@Autowired
	TipoProductoService tipoProductoService;
	
	
	/* - - -   Insertar Producto nuevo   - - - */
	@PostMapping("/producto")
	public ProductoResource registrarProducto(@RequestBody ProductoReducidoResource prr) throws Exception {
		/* ----- REQUEST --- */
		tipoProductoService.obtenerTipoProductoPorCodigo(prr.getCodigoTipoProducto());
		Producto producto =  new Producto();
		producto.setActivo(true);
		producto.setCodigo(prr.getCodigo());
		producto.setNombre(prr.getNombre());
		producto.setDescripcion(prr.getDescripcion());
		producto.setPrecio(prr.getPrecio());
		producto.setTipoProducto(tipoProductoService.obtenerTipoProductoPorCodigo(prr.getCodigoTipoProducto()));
		producto = productoService.guardarProducto(producto);	
		/* --- RESPONSE --- */
		ProductoResource productoResource = new ProductoResource();		
		productoResource.setId(producto.getId());
		productoResource.setNombre(producto.getNombre());
		productoResource.setCodigo(producto.getCodigo());
		productoResource.setDescripcion(producto.getDescripcion());
		productoResource.setPrecio(producto.getPrecio());
			TipoProductoResource tipoProductoResource=new TipoProductoResource();
			tipoProductoResource.setCodigo(producto.getTipoProducto().getCodigo());
			tipoProductoResource.setNombre(producto.getTipoProducto().getNombre());
		productoResource.setTipoProducto(tipoProductoResource);
		productoResource.setActivo(producto.getActivo());
		
		return productoResource;
	}
	
	
	
	
	/*  - - - Listar todos los productos - - - */
	@GetMapping("/productos")
	public  List<ProductoResource> obtenerProductos() throws Exception {
		List<ProductoResource> listado = new ArrayList<>();
		productoService.obtenerProductos().forEach(producto ->{
			ProductoResource productoResource =  new ProductoResource();
			productoResource.setId(producto.getId());
			productoResource.setNombre(producto.getNombre());
			productoResource.setCodigo(producto.getCodigo());			
			productoResource.setDescripcion(producto.getDescripcion());
			productoResource.setPrecio(producto.getPrecio());	
				TipoProductoResource tipoProductoResource=new TipoProductoResource();
				tipoProductoResource.setCodigo(producto.getTipoProducto().getCodigo());
				tipoProductoResource.setNombre(producto.getTipoProducto().getNombre());
			productoResource.setTipoProducto(tipoProductoResource);
			productoResource.setActivo(producto.getActivo());
			listado.add(productoResource);
		});
		return listado;
	}
	
	@Value("${igv}")
	BigDecimal igv;

	
	/* - - - -  Obtener un producto por id (obtener id por @PathVariable)   - - - - */
	@GetMapping("/productos/{id}")
	public ProductoIGVResource obtenerUnProducto(@PathVariable Long id) throws Exception {
		Producto producto = productoService.obtenerUnProducto(id);
		ProductoIGVResource productoIGVResource =  new ProductoIGVResource();
		productoIGVResource.setId(producto.getId());
		productoIGVResource.setNombre(producto.getNombre());
		productoIGVResource.setCodigo(producto.getCodigo());
		productoIGVResource.setPrecio(producto.getPrecio());
		productoIGVResource.setPrecioNeto(producto.getPrecio().add(producto.getPrecio().multiply(igv)));
		return productoIGVResource;
	}


}
