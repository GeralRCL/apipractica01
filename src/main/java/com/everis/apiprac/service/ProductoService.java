package com.everis.apiprac.service;


import com.everis.apiprac.model.entity.Producto;

public interface ProductoService {
	
	public Iterable<Producto> obtenerProductos() throws Exception;

	public Producto guardarProducto(Producto producto) throws Exception;

	public Producto obtenerUnProducto(Long id) throws Exception;

}
