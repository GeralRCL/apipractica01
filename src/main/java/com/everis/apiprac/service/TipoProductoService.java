package com.everis.apiprac.service;

import com.everis.apiprac.model.entity.TipoProducto;

public interface TipoProductoService {
	
	public TipoProducto obtenerTipoProductoPorCodigo(String codigo) throws Exception;

}
