package com.everis.apiprac.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apiprac.model.entity.Producto;
import com.everis.apiprac.model.repository.ProductoRepository;

@Service
public class ProductoServiceImpl implements ProductoService{

	@Autowired
	private ProductoRepository productoRepository;
	
	@Override
	public Iterable<Producto> obtenerProductos() throws Exception {	
		return productoRepository.findAll();
	}

	@Override
	public Producto guardarProducto(Producto producto) throws Exception {
		return productoRepository.save(producto);
	}

	@Override
	public Producto obtenerUnProducto(Long id) throws Exception {
		return productoRepository.findById(id).get();
	}

}
