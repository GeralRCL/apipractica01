package com.everis.apiprac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApipracApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApipracApplication.class, args);
	}

}
