package com.everis.apiprac.model.repository;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.apiprac.model.entity.Producto;

@Repository
public interface ProductoRepository extends CrudRepository<Producto, Long>{

}
