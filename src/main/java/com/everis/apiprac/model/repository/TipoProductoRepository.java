package com.everis.apiprac.model.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.apiprac.model.entity.TipoProducto;
// JpaRepository      CustomRepository

@Repository
public interface TipoProductoRepository extends CrudRepository<TipoProducto, Long>{
	
	public Optional<TipoProducto> findByCodigo(String codigo);
	
}
